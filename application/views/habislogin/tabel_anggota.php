<div id="content">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                </div>
            </nav>
            <h2>Anggota</h2>
            <div class="wrap_table_pendaftaran">
            <table class="table">
  <caption>List Anggota</caption>
  <thead>
    <tr>
      <th scope="col">Nama</th>
      <th scope="col">Nim</th>
      <th scope="col">Status</th>
      <th scope="col">Opsi</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($anggota as $data): ?>
    <tr>
      <td><?php echo $data->NAMA_ANGGOTA ?></td>
      <td><?php echo $data->NIM_ANGGOTA ?></td>
      <td><?php echo $data->STATUS_USER ?></td>
      <?php if($data->ID_STATUS_USER=='1'): ?>
      <td><a href="<?php echo site_url() ?>/home/nonaktif_anggota/<?php echo $data->ID_ANGGOTA ?>"><button type="button" class="btn btn-success">Dinonaktifkan</button></a><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ex<?php echo $data->ID_ANGGOTA?>">Detail</button></td>
      <?php elseif($data->ID_STATUS_USER=='2'):?>
      <td><a href="<?php echo site_url() ?>/home/aktif_anggota/<?php echo $data->ID_ANGGOTA ?>"><button type="button" class="btn btn-success">Diaktifkan</button></a><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ex<?php echo $data->ID_ANGGOTA?>">Detail</button></td>
      <?php endif; ?>
    </tr>
<?php endforeach; ?>
  </tbody>
</table>
</div>
</div>

<?php foreach($anggota as $data): ?>
<div class="modal fade" id="ex<?php echo $data->ID_ANGGOTA ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pendaftaran UKM <?php echo $data->NAMA_UKM ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="" method="post">
        <div class="form-group">
          <label>NIM</label>
          <input type="text" class="form-control" id="input_nim" name="input_nim" placeholder="<?php echo $data->NIM_ANGGOTA ?>" readonly>
        </div>
        <div class="form-group">
          <label>Nama</label>
          <input type="text" class="form-control" id="disabled_nama" name="disabled_nama" placeholder="<?php echo $data->NAMA_ANGGOTA ?>" readonly>
        </div>
        <div class="form-group">
          <label>Semester</label>
          <input type="text" class="form-control" id="disabled_semester" name="disabled_semester" placeholder="<?php echo $data->SEMESTER ?>" readonly>
        </div>
        <div class="form-group">
          <label>Tanggal Lahir</label>
          <input type="text" class="form-control" id="disabled_tanggal" name="disabled_tanggal" placeholder="<?php echo $data->TANGGAL_LAHIR ?> Lahir" readonly>
        </div>
        <div class="form-group">
          <label >Tempat Lahir</label>
          <input type="text" class="form-control" id="disabled_tempat" name="disabled_tempat" placeholder="<?php echo $data->TEMPAT_LAHIR ?>" readonly>
        </div>
        <div class="form-group">
          <label >Fakultas</label>
          <input type="text" class="form-control" id="disabled_fakultas" name="disabled_fakultas" placeholder="<?php echo $data->FAKULTAS ?>" readonly>
          <!-- <select class="form-control" id="disabled_fakultas" disabled>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </select> -->
        </div>
        <div class="form-group">
          <label >Prodi</label>
          <input type="text" class="form-control" id="disabled_prodi" name="disabled_prodi" placeholder="<?php echo $data->PRODI ?>" readonly>
          <!-- <select class="form-control" id="disabled_prodi" disabled>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </select> -->
        </div>
        <div class="form-group">
          <label >Alamat Tinggal</label>
          <textarea class="form-control" id="alamat_tinggal_anggota" name="alamat_tinggal_anggota" rows="3" readonly><?php echo $data->ALAMAT ?></textarea>
        </div>
        <div class="form-group">
          <label >Nomor Telepon</label>
          <input type="text" class="form-control" id="nomor_telepon_anggota" name="nomor_telepon_anggota" placeholder="<?php echo $data->NO_TELPON ?>" readonly>
        </div>
        <div class="form-group">
          <label >Deskripsi Diri</label>
          <textarea class="form-control" id="deskripsi_diri_anggota" name="deskripsi_diri_anggota" rows="6" readonly><?php echo $data->DESKRIPSI_DIRI ?></textarea>
        </div>
        <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="daftar_ukm">Daftar</button>
        </div> -->
      </form>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?>