<div id="content">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                </div>
            </nav>
            <h2>Tambah UKM</h2>
            
            <form action="<?php echo site_url() ?>/home/simpan_ukm" class="form_tambah_ukm" method="post">
  <div class="form-group">
    <label>Nama UKM</label>
    <input type="text" class="form-control" id="nama_ukm_baru" name="nama_ukm_baru" placeholder="Nama UKM">
  </div>
  <div class="form-group">
    <label>NIM Ketua UKM</label>
    <input type="text" class="form-control" id="nim_ketua_ukm" name="nim_ketua_ukm" placeholder="NIM Ketua UKM">
  </div>
  <div class="form-group">
    <label>Nama Ketua UKM</label>
    <input type="text" class="form-control" id="nama_ketua_ukm" name="nama_ketua_ukm" placeholder="Nama Ketua UKM" readonly>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

            </div>

<!-- <script>
$("#nomor_telepon_ketua").keydown(function (e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				// Allow: Ctrl/cmd+A
				(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				// Allow: Ctrl/cmd+C
				(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
				// Allow: Ctrl/cmd+X
				(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
				// Allow: home, end, left, right
				(e.keyCode >= 35 && e.keyCode <= 39)) {
				// let it happen, don't do anything
				return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});
</script> -->