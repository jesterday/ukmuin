<div id="content">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                </div>
            </nav>
            <h2>Form Pendaftaran</h2>
            <?php foreach($daftar as $data):?>
            <form action="<?php echo site_url() ?>/home/update_desk/<?php echo $data->ID_UKM ?>" class="form_tambah_ukm" method="post">
  <div class="form-group">
    <label>Deskripsi Diri</label>
    <br/>
    <label style="font-size:12px;">Tambahkan keterangan untuk pendaftar jika form kurang memadai</label>
    <textarea class="form-control" id="isi_des_textarea" name="isi_des_textarea" rows="6"><?php echo $data->DESKRIPSI_PENDAFTAR ?></textarea>
  </div>

  <div class="form-group" style="width:20%;float:left;">
    <label>Tanggal Buka Pendaftaran</label>
    <input type="text" class="form-control-file" id="tanggal_buka" name="tanggal_buka" style="width:100%">
  </div>
  <div class="form-group" style="width:2.5%;float:left;margin-left:2.5%">
  <br/>
  <label>Sampai</label>
  </div>
  <div class="form-group" style="width:20%;float:left;margin-left:2.5%; margin-right:1%;">
    <br/>
    <input type="text" class="form-control-file" id="tanggal_tutup" name="tanggal_tutup" style="width:100%">
  </div>
  <br/>

  <!-- <div id="drop_gambar_berita" name="drop_gambar_berita" class="drop_gambar">
      <input type="file" class="form-control" id="file_gambar_berita" accept="image/*">
      <img id="prev_foto_berita" src="" class="img-responsive img-thumbnail" alt="Preview Image" style="width:100%;">
  </div> -->
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
<?php endforeach; ?>
            </div>
<script>
$(document).ready(function() {
    $( "#tanggal_buka" ).datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $( "#tanggal_tutup" ).datepicker({
        dateFormat: 'yy-mm-dd'
    });
} );


</script>
