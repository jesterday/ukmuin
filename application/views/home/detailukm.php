<div class="events page_section" style="margin-top: 15%;">
    <div class="container">
      <?php foreach ($detail as $data): ?>
      <div class="row">
        <div class="col">
          <div class="section_title text-center" style="margin-bottom: 5%;">
            <h1><?php echo $data->NAMA_UKM ?></h1>
          </div>
        </div>
      </div>

      <div class="row event_item">
        <div class="col">
          <div class="row d-flex flex-row align-items-end">

            <div class="col-lg-12 order-lg-3 order-1">
              <div class="event_image berita_full">
                <!-- <img src="<?php //echo base_url() ?>assets/images/ukor.png"/> -->
                <img src="<?php echo base_url() ?>assets/uploads/<?php echo $data->FOTO_PROFIL_UKM ?>" class="profil_ukm"/>
              </div>

              <div class="event_content">
                <div class="event_isi"><?php echo $data->PROFIL_UKM ?></div>
              </div>
            </div>

          </div>

        </div>
      </div>
      <?php endforeach; ?>
      <div class="row">
        <div class="col">
          <div class="section_title text-center" style="margin-bottom: 5%;">
            <h1>Dokumentasi</h1>
          </div>
        </div>
      </div>
        <div class="container_image_dokumentasi">
        <?php foreach($foto as $data): ?>
            <img src="<?php echo base_url() ?>assets/dokumentasi/<?php echo $data->UPLOAD_GAMBAR1 ?>" style="width:32.5%;margin-bottom:10px;" />
        <?php endforeach; ?>
            <!-- <img src="<?php echo base_url() ?>assets/images/ukor.png" style="width:32.5%;" />
            <img src="<?php echo base_url() ?>assets/images/ukor.png" style="width:32.5%;" /> -->
        </div>
    </div>
  </div>
</div>