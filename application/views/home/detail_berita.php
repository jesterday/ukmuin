<div class="events page_section" style="margin-top: 15%;">
    <div class="container">
      <?php foreach($berita as $row): ?>
      <div class="row">
        <div class="col">
          <div class="section_title text-center" style="margin-bottom: 5%;">
            <h1><?php echo $row->JUDUL_BERITA ?></h1>
          </div>
        </div>
      </div>

      <div class="row event_item">
        <div class="col">
          <div class="row d-flex flex-row align-items-end">

            <div class="col-lg-12 order-lg-3 order-1">
              <div class="event_image berita_full">
                <img src="<?php echo base_url() ?>assets/images/ukor.png"/>
              </div>

              <div class="event_content">
                <div class="event_isi">
                <?php echo $row->ISI_BERITA ?>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
<?php endforeach; ?>
      <!-- <div class="row">
        <div class="col">
          <div class="section_title text-center" style="margin-bottom: 5%;">
            <h1>Dokumentasi</h1>
          </div>
        </div>
      </div>
        <div class="container_image_dokumentasi">
            <img src="<?php //echo base_url() ?>assets/images/ukor.png" style="width:32.5%;" />
            <img src="<?php //echo base_url() ?>assets/images/ukor.png" style="width:32.5%;" />
            <img src="<?php //echo base_url() ?>assets/images/ukor.png" style="width:32.5%;" />
        </div>
    </div> -->
  </div>
</div>