<div class="events page_section" style="margin-top: 15%;">
    <div class="container">

      <?php foreach($berita as $data):?>
      <!-- <?php
        // $potong = $data->ISI_BERITA;
        // $pos = strpos($potong, "</p>");
        // $kata = substr($potong,$pos+4,7000);
      ?> -->
      <div class="row">
        <div class="col">
          <div class="section_title text-center" style="margin-bottom: 5%;">
            <h1><?php echo $data->JUDUL_BERITA;?></h1>
          </div>
        </div>
      </div>

      <div class="row event_item">
        <div class="col">
          <div class="row d-flex flex-row align-items-end">

            <div class="col-lg-12 order-lg-3 order-1">
              <div class="event_image berita_full">
                <img src="<?php echo base_url() ?>assets/uploads/<?php echo $data->GAMBAR_BERITA ?>"/>
              </div>

              <div class="event_content">
                <div class="event_isi"><?php echo $data->ISI_BERITA;?></div>
              </div>
              <div class="file_content">
                <label style="font-size:24px;">File</label>
                <a href="<?php echo base_url() ?>assets/dokumen/<?php echo $data->FILE_BERITA ?>"><div class="event_file" ><img src="<?php echo base_url() ?>assets/icon/file.png" style="width:100px;" /><?php echo $data->FILE_BERITA;?></div></a>
              </div>

            </div>


          </div>

        </div>
      </div>
    <?php endforeach;?>

    </div>
  </div>
</div>