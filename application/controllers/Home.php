<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
		parent::__construct();
   		$this->load->database('default',TRUE);
   		$this->load->model('Beranda');
		$this->load->helper(array('form','url','file','download','date'));
		error_reporting(E_ALL^(E_NOTICE | E_WARNING));
	}
	public function getprodi_json(){
		header('Content-Type: application/json');
		echo json_encode($this->Beranda->get_prodi($prodi));
	}
	public function get_ukm_json(){
		header('Content-Type: application/json');
		echo json_encode($this->Beranda->get_data_ukm());
	}
	// public function getusername_json(){
	// 	$data = file_get_contents('http://eis.uinsby.ac.id/eis/login/'.$username.'/'.$password);
	// 	print_r($data);
	// }
	public function simpan_anggota($id_ukm){
		$data=array(
			'ID_UKM'=> $id_ukm,
			'ID_STATUS_USER'=>3,
			'NAMA_ANGGOTA'=>$this->input->post('disabled_nama'),
			'NIM_ANGGOTA'=>$this->input->post('input_nim'),
			'FAKULTAS'=>$this->input->post('disabled_fakultas'),
			'PRODI'=>$this->input->post('disabled_prodi'),
			'SEMESTER'=>$this->input->post('disabled_semester'),
			'TANGGAL_LAHIR'=>$this->input->post('disabled_tanggal'),
			'TEMPAT_LAHIR'=>$this->input->post('disabled_tempat'),
			'ALAMAT'=>$this->input->post('alamat_tinggal_anggota'),
			'NO_TELPON'=>$this->input->post('nomor_telepon_anggota'),
			'DESKRIPSI_DIRI'=>$this->input->post('deskripsi_diri_anggota')
		);
		$this->Beranda->simpan('anggota_ukm',$data);
		redirect(base_url());
	}
	public function update_status_terima($id_anggota){
		$data=array(
			'ID_STATUS_USER'=>'1'
		);
		$this->db->where('ID_ANGGOTA',$id_anggota);
		$this->db->update('anggota_ukm',$data);
		// $this->Beranda->update_status($id_anggota);
		redirect(site_url('/home/tabel_pendaftaran'));
	}
	public function update_status_tditerima($id_anggota){
		// $data=array(
		// 	'ID_STATUS_USER'=>'1'
		// );
		$this->db->where('ID_ANGGOTA',$id_anggota);
		$this->db->delete('anggota_ukm');
		// $this->Beranda->delete_pendaftaran($id_anggota);
		redirect(site_url('/home/tabel_pendaftaran'));
	}
	public function nonaktif_anggota($id_anggota){
		$data=array(
			'ID_STATUS_USER'=>'2'
		);
		$this->db->where('ID_ANGGOTA',$id_anggota);
		$this->db->update('anggota_ukm',$data);
		// $this->Beranda->nonaktif_anggota($id_anggota);
		redirect(site_url('/home/tabel_anggota'));
	}
	public function aktif_anggota($id_anggota){
		$data=array(
			'ID_STATUS_USER'=>'1'
		);
		$this->db->where('ID_ANGGOTA',$id_anggota);
		$this->db->update('anggota_ukm',$data);
		// $this->Beranda->aktif_anggota($id_anggota);
		redirect(site_url('/home/tabel_anggota'));
	}
	public function tambah_berita($id_ukm){
		$now=date('Y-m-d');
		$data1=array();
		$config['upload_path'] = './assets/uploads/';
		$config['allowed_types'] = 'jpg|png|jpeg';
   		$config['max_size']  = '5120000';
   		$config['remove_space'] = TRUE;
		$this->load->library('upload');
		$this->upload->initialize($config);
		if ( !$this->upload->do_upload('file_gambar_berita')){
			$error = array('error' => $this->upload->display_errors());
			print_r($error);
        }else{
			unset($config);
			$config['upload_path'] = './assets/dokumen/';
			$config['allowed_types'] = 'pdf';
   			$config['max_size']  = '5120000';
			$config['remove_space'] = TRUE;
			$this->upload->initialize($config);
			if(!$this->upload->do_upload('file_file_berita')){
				$data=array(
					'ID_UKM'=>$id_ukm,
					'JUDUL_BERITA'=>$this->input->post('judul_berita'),
					'ISI_BERITA'=>$this->input->post('isi_berita_textarea'),
					'TANGGAL_BERITA'=>$now,
					'GAMBAR_BERITA'=>$this->upload->data('file_name')
				);
				// print_r($data);
				$this->Beranda->simpan('berita',$data);
				redirect(site_url('/home/berita'));
			}else{	
				$data=array(
					'ID_UKM'=>$id_ukm,
					'JUDUL_BERITA'=>$this->input->post('judul_berita'),
					'ISI_BERITA'=>$this->input->post('isi_berita_textarea'),
					'TANGGAL_BERITA'=>$now,
					'GAMBAR_BERITA'=>$_FILES['file_gambar_berita']['name'],
					'FILE_BERITA'=>$_FILES['file_file_berita']['name']
				);
				// print_r($data);
				$this->Beranda->simpan('berita',$data);
				redirect(site_url('/home/berita'));
			}
			
		}
	}
	public function hapus_berita($id_berita){
		$this->db->where('ID_BERITA',$id_berita);
		$this->db->delete('berita');
		// $this->Beranda->delete_berita($id_berita);
		redirect(site_url('/home/berita'));
	}
	public function update_profil($id_ukm){
		$data = array();
   		$config['upload_path'] = './assets/uploads/';
   		$config['allowed_types'] = 'jpg|png|jpeg|JPG|PNG';
   		$config['max_size']  = '5120000';
   		$config['remove_space'] = TRUE;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// print_r($_FILES['tes_gambar']);
		// echo $_FILES['tes_gambar']['name'];
		// print_r($this->upload->do_upload('tes_gambar'));
		if ( !$this->upload->do_upload('tes_gambar')){
			$error = array('error' => $this->upload->display_errors());
			print_r($error);
        }
        else{
			$data = array(
				'PROFIL_UKM'=>$this->input->post('profile_ukm_baru'),
				'FOTO_PROFIL_UKM'=>$this->upload->data('file_name')
			);
			$this->db->where('ID_UKM',$id_ukm);
			$this->db->update('ukm',$data);
			redirect(site_url('/home/update_profil_ukm'));
			// print_r($data);
        }
	}
	public function update_dokumentasi($id_ukm){
		$data = array();
		// $i=0;
   		$config['upload_path'] = './assets/dokumentasi/';
   		$config['allowed_types'] = 'jpg|png|jpeg';
   		$config['max_size']  = '5120000';
   		$config['remove_space'] = TRUE;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// print_r($_FILES['tes_gambar']);
		// echo $_FILES['tes_gambar']['name'];
		// $count = count($_FILES['upload_dokumentasi1']['name']);
		// print_r($count);
		if(!$this->upload->do_upload('upload_dokumentasi1')){
				$error = array('error' => $this->upload->display_errors());
				print_r($error);
		}else{
			$data = array(
				'ID_UKM'=>$id_ukm,
				'UPLOAD_GAMBAR1'=>$this->upload->data('file_name')
			);
				// print_r($data);
			$this->Beranda->simpan('dokumentasi',$data);
				// $this->db->where('ID_UKM',$id_ukm);
				// $this->db->update('dokumentasi',$data);
			redirect(site_url('/home/update_profil_ukm'));
		}
			// $data1=$_FILES['upload_dokumentasi'.$i]['name'];
			// print_r($data1);
			// // $upload1=$this->upload->do_upload('upload_dokumentasi1');
			// print_r($this->upload->do_upload('upload_dokumentasi1'));
		
	}
	public function index()
	{
		$file = file_get_contents(site_url()."/home/data_pendaftaran");
		$arr = json_decode($file,true);
		// print_r($arr);
		// $banyak = count($arr);
        // for ($i=0; $i < $banyak; $i++) {
        //   if ($arr[$i][nim] == $nim) {
        //     $nimS = $arr[$i][nim];
		// 	$namaS = $arr[$i][nama];
		// 	$tanggal = $arr[$i][tgllahir];
		// 	$lahirS = $arr[$i][tmplahir];
		// 	$kodeS = $arr[$i][kodeunit];
		// 	$semesterS = $arr[$i][semestermhs];
        //     $kodepS = $arr[$i][kodeunitparent];
        //     $prodiS = $arr[$i][namaunit];
        //     // $lulusS = $arr[$i][periodelulus];
        //     // $prodiS = $arr[$i][namaunit];
        //     // $fakS = $arr[$i][fakultas];
        //     break;
        //   }
        // }
		// $data['fakultas'] = $this->Beranda->get_fakultas();
		// print_r($data['fakultas']);
		// echo json_encode($this->Beranda->get_prodi());
		$data['ukm'] = $this->Beranda->get_all_ukm();
		$this->load->view('base/head');
		$this->load->view('home/topbar');
		$this->load->view('home/slider');
		$this->load->view('home/isi_berita');
		$this->load->view('home/home');
		$this->load->view('home/ukm',$data);
		$this->load->view('base/foot');
	}

	// public function indexlogin(){
	// 	$this->load->view('base/head_sidebar');
	// 	$this->load->view('habislogin/sidebar');
	// 	$this->load->view('habislogin/welcome');
	// 	$this->load->view('base/foot_sidebar');
	// }

	public function tabel_anggota(){
		$id=$this->session->userdata('username');
		$nama=$this->session->userdata('nama_ketua');
		$id_ukm=$this->session->userdata('id');
		$data['anggota']=$this->Beranda->get_all_anggota($id_ukm);
		if($id!=null){
			$this->load->view('base/head_sidebar');
			$this->load->view('habislogin/sidebar');
			$this->load->view('habislogin/tabel_anggota',$data);
			$this->load->view('base/foot_sidebar');
		}else{
			redirect(base_url());
		}
		
	}

	public function berita(){
		$id=$this->session->userdata('username');
		$nama=$this->session->userdata('nama_ketua');
		$id_ukm=$this->session->userdata('id');
		$data['berita']=$this->Beranda->get_berita_ukm($id_ukm);
		if($id!=null){
			$this->load->view('base/head_sidebar');
			$this->load->view('habislogin/sidebar');
			$this->load->view('habislogin/buat_berita',$data);
			$this->load->view('base/foot_sidebar');
		}else{
			redirect(base_url());
		}
	}
	public function update_profil_ukm(){
		$user=$this->session->userdata('username');
		$id=$this->session->userdata('id');
		// $nama=$this->session->userdata('nama_ketua');
		// print_r($id);
		$data['dokumentasi']=$this->Beranda->get_dokumentasi_ukm($id);
		$data['ukm']=$this->Beranda->get_profil_ukm($id);
		// print_r($data['dokumentasi']);
		if($user!=null){
			$this->load->view('base/head_sidebar');
			$this->load->view('habislogin/sidebar');
			$this->load->view('habislogin/profil_ukm',$data);
			$this->load->view('base/foot_sidebar');
		}else if($user==null){
			redirect(base_url());
		}
	}

	public function tabel_pendaftaran(){
		$id=$this->session->userdata('username');
		$nama=$this->session->userdata('nama_ketua');
		//$data['detail']=$this->Beranda->get_all_data_pendaf();	
		$data['daftar']=$this->Beranda->get_all_daftar();
		if($id!=null){
			$this->load->view('base/head_sidebar');
			$this->load->view('habislogin/sidebar');
			$this->load->view('habislogin/tabel_pendaftar',$data);
			$this->load->view('base/foot_sidebar');
		}else{
			redirect(base_url());
		}
		
	}
	public function atur_pendaftaran(){
		$id=$this->session->userdata('username');
		$nama=$this->session->userdata('nama_ketua');
		$id_ukm=$this->session->userdata('id');
		//$data['detail']=$this->Beranda->get_all_data_pendaf();	
		$data['daftar']=$this->Beranda->get_des_daftar($id_ukm);
		if($id!=null){
			$this->load->view('base/head_sidebar');
			$this->load->view('habislogin/sidebar');
			$this->load->view('habislogin/pengaturan_daftar',$data);
			$this->load->view('base/foot_sidebar');
		}else{
			redirect(base_url());
		}
	}
	public function update_desk($id_ukm){
		$data=array(
			'DESKRIPSI_PENDAFTAR'=>$this->input->post('isi_des_textarea'),
			'TANGGAL_BUKA'=>$this->input->post('tanggal_buka'),
			'TANGGAL_TUTUP'=>$this->input->post('tanggal_tutup'),
		);
		$this->db->where('ID_UKM',$id_ukm);
		$this->db->update('ukm',$data);
		redirect(site_url('/home/atur_pendaftaran'));
	}

	public function tambah_ukm(){
		$id=$this->session->userdata('username');
		$jastruk=$this->session->userdata('jastruk');
		$file = file_get_contents(site_url()."/home/data_pendaftaran");
		$arr = json_decode($file,true);
		if($id!=null && $jastruk=='364'){
			$this->load->view('base/head_sidebar');
			$this->load->view('habislogin/sidebar_admin');
			$this->load->view('habislogin/tambah_ukm');
			$this->load->view('base/foot_sidebar');
		}else{
			redirect(base_url());
		}
	}
	public function simpan_ukm(){
		$data=array(
			'ID_STATUS_UKM'=>'3',
			'NAMA_UKM'=>$this->input->post('nama_ukm_baru'),
			'NIM_KETUA_UKM'=>$this->input->post('nim_ketua_ukm'),
			'NAMA_KETUA_UKM'=>$this->input->post('nama_ketua_ukm'),
			'PROFIL_UKM'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse bibendum enim et odio faucibus, non ullamcorper lacus consectetur.',
			'DESKRIPSI_PENDAFTAR'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse bibendum enim et odio faucibus, non ullamcorper lacus consectetur.'
			);
		$this->Beranda->simpan('ukm',$data);
		redirect(site_url('/home/tambah_ukm'));
	}

	public function tabel_ukm(){
		$id=$this->session->userdata('username');
		$jastruk=$this->session->userdata('jastruk');
		$data['anggota']=$this->Beranda->get_all_anggota();
		$data['ukm']=$this->Beranda->get_data_ukm();
		if($id!=null && $jastruk=='364'){
			$this->load->view('base/head_sidebar');
			$this->load->view('habislogin/sidebar_admin');
			$this->load->view('habislogin/tabel_ukm',$data);
			$this->load->view('base/foot_sidebar');
		}else{
			redirect(base_url());
		}
	}
	public function update_ketua_ukm($id_ukm){
		$data=array(
			'NIM_KETUA_UKM'=>$this->input->post('nim_ketua_ukm'),
			'NAMA_KETUA_UKM'=>$this->input->post('nama_ketua_ukm')
		);
		// print_r($data);
		$this->db->where('ID_UKM',$id_ukm);
		$this->db->update('ukm',$data);
		redirect(site_url('/home/tabel_ukm'));
	}
	public function update_status_beku($id_ukm){
		$data=array(
			'ID_STATUS_UKM'=>'2'
		);
		$this->db->where('ID_UKM',$id_ukm);
		$this->db->update('ukm',$data);
		redirect(site_url('/home/tabel_ukm'));
	}
	public function update_status_unfreeze($id_ukm){
		$data=array(
			'ID_STATUS_UKM'=>'1'
		);
		$this->db->where('ID_UKM',$id_ukm);
		$this->db->update('ukm',$data);
		redirect(site_url('/home/tabel_ukm'));
	}

	public function detail_ukm($id_ukm){
		$data['detail'] = $this->Beranda->get_detail_ukm($id_ukm);
		$data['foto'] = $this->Beranda->get_dokumentasi_ukm($id_ukm);
		// print_r($data['detail']);
		$this->load->view('base/head');
		$this->load->view('home/topbar');
		$this->load->view('home/detailukm',$data);
		$this->load->view('base/foot');
	}

	// public function tabel_anggota_admin(){
	// 	$this->load->view('base/head_sidebar');
	// 	$this->load->view('habislogin/sidebar_admin');
	// 	$this->load->view('habislogin/tabel_anggota_admin');
	// 	$this->load->view('base/foot_sidebar');
	// }

	// public function form(){
	// 	$this->load->view('base/head');
	// 	$this->load->view('form/form_head');
	// 	$this->load->view('base/foot');
	// }

	// public function isi_berita($id_berita){
	// 	$data['berita'] = $this->Beranda->tampil_berita($id_berita);
	// 	$this->load->view('base/head');
	// 	$this->load->view('home/berita',$data);
	// 	$this->load->view('base/foot');
	// }

	public function isi_berita($id_berita){
		$data['berita'] = $this->Beranda->tampil_berita($id_berita);
		$this->load->view('base/head');
		$this->load->view('home/topbar');
		$this->load->view('home/berita',$data);
		$this->load->view('base/foot');
	}

	public function data_pendaftaran(){
		// header('Content-type: application/json');
		// $data = file_get_contents('http://eis.uinsby.ac.id/eis/tes_json/'.$username.'');
		$data = file_get_contents("http://eis.uinsby.ac.id/eis/tes_json/");
		echo $data;
		// print_r(json_decode($data,true));
		// echo $data;
		// $obj = json_decode($data,true);
		// echo $obj;
		// print_r($obj);
		// echo $obj[0]['nim'];
		// http://eis.uinsby.ac.id/eis/tes_json
	}

	function update_ukm(){
		$data = array();
   		$config['upload_path'] = './assets/uploads/';
   		$config['allowed_types'] = 'jpg|png|jpeg';
   		$config['max_size']  = '2048000';
   		$config['remove_space'] = TRUE;
   		$this->load->library('upload', $config);
   		if ( !$this->upload->do_upload('dokumen')){
    		$error = array('error' => $this->upload->display_errors());
    		return $error;
   		}else{
			$data = $this->upload->data('file_name');
			$data1 = array(
				'isi_berita'=>$this->input->post('isi'),
				'judul'=>$this->input->post('judul'),
				'status_berita'=>$this->input->post('publish'),
				'tgl_berita'=>$now,
				'status'=>$this->input->post('status'),
				'GAMBAR'=>$data
			  );
    	}
	}
	public function logout(){
		$user_data = $this->session->all_userdata();
    	$this->session->sess_destroy();
    	redirect(base_url());
	}

	public function login(){
		$username = trim($this->input->post('nim'));
		$password = trim($this->input->post('passwordlogin'));
		// $md5pass=md5($password);
		$data = file_get_contents('http://eis.uinsby.ac.id/eis/login_json/'.$username.'/'.$password);
		$data2 = file_get_contents('http://eis.uinsby.ac.id/eis/login_peg/'.$username.'/'.$password);
		$data1['status']=$this->Beranda->get_nim($username);
		$obj = json_decode($data, true);
		$obj1 = json_decode($data2,true);
		// print_r($obj1[0][id_jastruk]);
		if(!empty($data1['status'])){
			if(!empty($obj[0][nim]))
            {
				// print_r($data1['status']);
				foreach($data1['status'] as $row ){
					$username=$row->NIM_KETUA_UKM;
					$id=$row->ID_UKM;
					$nama=$row->NAMA_KETUA_UKM;
				}
				// print_r($user);
				// $obj[0][nim];
				$session_user=array(
					'username'=>$username,
					'id'=>$id,
					'nama'=>$nama
				);
				// print_r($session_user);
				$this->session->set_userdata($session_user);
				// // print_r($session_user);
				redirect(site_url("/home/update_profil_ukm"));
            }
            else{
				// print_r($obj[0][nim]);
                redirect(base_url());
			}
				// print_r($data1['status']);	
		}
		else if(!empty($obj1[0][nip])){
			$username=$obj1[0][nip];
			$jastruk=$obj1[0][id_jastruk];
			$session_user=array(
				'username'=>$username,
				'jastruk'=>$jastruk
			);
			$this->session->set_userdata($session_user);
			// print_r($session_user);
			redirect(site_url("/home/tabel_ukm"));
		}
		// else if(!empty($data2)){

		// }
		else{
			redirect(base_url());
		}
				// print_r($data1['status'][0]['NIM_KETUA_UKM']);
						// print_r($obj[0][nim]);
					// 	foreach($data1['status'] as $row){
					// 		$user=$row->NIM_KETUA_UKM;
					// 		// $nama=$row->NAMA_KETUA_UKM;
					// 	}
					// 	// print_r($user);
					// 	$session_user=array(
					// 		'user'=>$username,
					// 		// 'nama'=>$nama_ketua
					// 	);
					// 	$this->session->set_userdata($session_user);
					// 	print_r($session_user);
                	// 	// redirect(site_url("/home/update_profil_ukm"));
					// }
            		// else{
					// 	// print_r($obj[0][nim]);
					// 	redirect(base_url());
					// }
					// print_r($obj[0][nim]);
			// if(!empty($stat2['status'])){
			// 	foreach($stat2['status'] as $row){
			// 	  $username = $row['username'];
			// 	}
			// 	$session_user=array(
			// 	  'username'=>$username
			// 	);
			// 	$this->session->set_userdata($session_user);
			// 	// print_r($session_user);
			// 	redirect(base_url("admin/index"));
			//   }
    }



	public function get_loadmore() {
		$page   =  $_GET['page'];
		$query = $this->Beranda->get_berita_more($page);

		foreach($query as $data){
		   $potong = $data->ISI_BERITA;
		   $pos = strpos($potong, "</p>");
		   $kata = substr($potong,$pos+4,7000);
		   // $pos1 = strpos($kata, "</p>");
		   // $kata1 = substr($kata,$pos1+4,7000);

		   echo "<br />";
		  echo '
		  <div class="row event_item">
			<div class="col">
			  <div class="row d-flex flex-row align-items-end align-items-center">

				<div class="col-lg-2 order-lg-1 order-2">
				  <div class="event_date d-flex flex-column align-items-center justify-content-center">
					<div class="event_date_sql hidden" style="display:none">'.$data->TANGGAL_BERITA.'</div>
					<div class="event_day"></div>
					<div class="event_month"></div>
				  </div>
				</div>

				<div class="col-lg-6 order-lg-2 order-3">
				  <div class="event_content">
				  <div class="event_name"><a class="trans_200" href="'.site_url().'/home/isi_berita/'.$data->ID_BERITA.'">'.$data->JUDUL_BERITA.'</a></div>
				  <div class="event_isi">'.$kata.'</div>

				  </div>

				</div>

				<div class="col-lg-4 order-lg-3 order-1">
				  <div class="event_image">
				   <!--<img src="http://beta.uinsby.ac.id/assets/uploads/gambar/1/'.$data->GAMBAR.'"/>
				  </div>

				</div>

			  </div>

			</div>
		  </div>
		  ';
		}
		exit;
	}
}
