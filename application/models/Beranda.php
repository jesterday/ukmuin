<?php
  class Beranda extends CI_Model{
    function __construct(){
      parent::__construct();
      $this->load->database('default',TRUE);
    }
    
    // function get_fakultas(){
    //     $query = $this->db->query("SELECT kodeunit,namaunit FROM ms_unit WHERE kodeunitparent='UIN'");
    //     return $query->result();
    // }

    // function get_prodi(){
    //   $query = $this->db->query("SELECT kodeunit,namaunit FROM ms_unit where kodeunitparent='A' OR kodeunitparent='B' OR kodeunitparent='C' OR kodeunitparent='D' OR kodeunitparent='E' OR kodeunitparent='G' OR kodeunitparent='H' OR kodeunitparent='I' OR kodeunitparent='J' ORDER BY namaunit asc");
    //   $result = $query->result();
    //   return $result;
    // }

    function get_berita_more($page)
    {
        if ($page == 0) {
          $offset = 0;
        }
        else {
          $offset = $page * 3;
        }
        // $offset = $page * 3;
        $limit  = 3;
        $query  = $this->db->query("SELECT ID_BERITA,JUDUL_BERITA,GAMBAR_BERITA,TANGGAL_BERITA,ISI_BERITA FROM berita order by TANGGAL_BERITA DESC LIMIT $offset, $limit");
        $result = $query->result();
        return $result;
    }
    function tampil_berita($id_berita){
      $query=$this->db->query("SELECT ID_BERITA,JUDUL_BERITA,GAMBAR_BERITA,TANGGAL_BERITA,ISI_BERITA,FILE_BERITA FROM berita WHERE ID_BERITA = ".$id_berita);
      return $query->result();
    }
    function get_all_ukm(){
      $query=$this->db->query("SELECT ID_UKM,NAMA_UKM,DESKRIPSI_PENDAFTAR FROM ukm WHERE ID_STATUS_UKM = 1");
      return $query->result();
    }
    function get_detail_ukm($id_ukm=""){
      $query=$this->db->query("SELECT NAMA_UKM,PROFIL_UKM,FOTO_PROFIL_UKM FROM ukm where ID_UKM='$id_ukm'");
      return $query->result();
    }
    function get_dokumentasi_ukm($id_ukm){
      $query=$this->db->query("SELECT ID_DOKUMENTASI,ID_UKM,UPLOAD_GAMBAR1 FROM dokumentasi where ID_UKM='$id_ukm'");
      return $query->result();
    }
    function simpan($table,$data){
      $this->db->insert($table,$data);
      $param = $this->db->affected_rows();
      return $param;
    }
    function get_nim($nim){
      $query=$this->db->query("SELECT NIM_KETUA_UKM,NAMA_KETUA_UKM,ID_UKM FROM ukm WHERE NIM_KETUA_UKM='$nim'");
      return $query->result();
    }
    // function simpan_data(){
    //   $this->db->insert($table,$data);
    //   $this->db->where();
    //   $param = $this->db->affected_rows();
    //   return $param;
    // }
    function get_all_berita(){
      $query=$this->db->query("SELECT ID_BERITA,ID_UKM,JUDUL_BERITA,TANGGAL_BERITA FROM berita ORDER BY TANGGAL_BERITA DESC");
      return $query->result();
    }
    function get_berita_ukm($id){
      $query=$this->db->query("SELECT ID_BERITA,ID_UKM,JUDUL_BERITA,TANGGAL_BERITA FROM berita WHERE ID_UKM='$id' ORDER BY TANGGAL_BERITA DESC");
      return $query->result();
    }
    function get_data_ukm(){
      $query=$this->db->query("SELECT a.ID_UKM,a.NAMA_UKM,a.NAMA_KETUA_UKM,a.NIM_KETUA_UKM,b.ID_STATUS_UKM,b.STATUS_UKM FROM ukm a JOIN status_ukm b on a.ID_STATUS_UKM = b.ID_STATUS_UKM");
      return $query->result();
    }
    function get_all_daftar(){
      $query=$this->db->query("SELECT a.*, b.STATUS_USER,b.ID_STATUS_USER FROM anggota_ukm a JOIN status_anggota b on a.ID_STATUS_USER = b.ID_STATUS_USER where b.ID_STATUS_USER='3'");
      return $query->result();
    }
    function get_all_anggota($id_ukm){
      $query=$this->db->query("SELECT a.*,b.STATUS_USER,b.ID_STATUS_USER,c.ID_UKM FROM anggota_ukm a JOIN status_anggota b on a.ID_STATUS_USER = b.ID_STATUS_USER JOIN ukm c ON c.ID_UKM=a.ID_UKM where b.ID_STATUS_USER='1' OR b.ID_STATUS_USER='2' AND c.ID_UKM='$id'");
      return $query->result();
    }
    // function update_status($id_anggota){
    //   $query=$this->db->query("UPDATE anggota_ukm SET ID_STATUS_USER = '1' WHERE ID_ANGGOTA='$id_anggota'");
    //   return $query->result();
    // }
    // function delete_pendaftaran($id_anggota){
    //   $query=$this->db->query("DELETE FROM anggota_ukm WHERE ID_ANGGOTA='$id_anggota'");
    //   return $query->result();
    // }
    // function delete_berita($id_berita){
    //   $query=$this->db->query("DELETE FROM berita WHERE ID_BERITA='$id_berita'");
    //   return $query->result();
    // }
    // function nonaktif_anggota($id_anggota){
    //   $query=$this->db->query("UPDATE anggota_ukm SET ID_STATUS_USER = '2' WHERE ID_ANGGOTA='$id_anggota'");
    //   return $query->result();
    // }
    // function aktif_anggota($id_anggota){
    //   $query=$this->db->query("UPDATE anggota_ukm SET ID_STATUS_USER = '1' WHERE ID_ANGGOTA='$id_anggota'");
    //   return $query->result();
    // }
    // function update_profil_ukm($update_profil,$id_ukm){
    //   $query=$this->db->query("UPDATE ukm SET PROFIL_UKM='$update_profil' WHERE ID_UKM='$id_ukm'");
    //   return $query->return();
    // }
    function get_profil_ukm($id_ukm){
      $query=$this->db->query("SELECT ID_UKM, PROFIL_UKM, FOTO_PROFIL_UKM FROM ukm WHERE ID_UKM='$id_ukm'");
      return $query->result();
    }
    function get_des_daftar($id_ukm){
      $query=$this->db->query("SELECT ID_UKM, DESKRIPSI_PENDAFTAR FROM ukm WHERE ID_UKM='$id_ukm'");
      return $query->result();
    }
    
  }
?>